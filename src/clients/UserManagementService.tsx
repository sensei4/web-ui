import dayjs from 'dayjs';
import { AuthenticationResponse } from 'types/AuthenticationResponse';
import { NewUser, User } from 'types/User';

const userManagementServiceUrl = process.env.REACT_APP_USER_SERVICE_URL;

function getHeaders(token: string) {
  return {
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
    },
  };
}

export async function registerUser(
  newUser: NewUser
): Promise<AuthenticationResponse> {
  const formattedNewUser = {
    ...newUser,
    dateOfBirth: newUser.dateOfBirth.format('YYYY-MM-DD'), //Formatting
  };
  return fetch(`${userManagementServiceUrl}/api/v1/auth/register`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(formattedNewUser),
  }).then((response) => {
    if (response.status === 201) return response.json();
    else if (response.status === 409)
      throw new Error('An account already exists with this email address');

    return Error('Something went wrong, please try again later');
  });
}

export async function authenticateUser(
  email: string,
  password: string
): Promise<Response> {
  return fetch(`${userManagementServiceUrl}/api/v1/auth/login`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ email, password }),
  });
}

export async function getAllUsers(token: string): Promise<User[]> {
  const response = await fetch(
    `${userManagementServiceUrl}/api/v1/users`,
    getHeaders(token)
  );
  return (await response.json()) as User[];
}

export async function getUser(id: string, token: string): Promise<User> {
  const response = await fetch(
    `${userManagementServiceUrl}/api/v1/users/${id}`,
    getHeaders(token)
  );
  return (await response.json()) as User;
}

export async function updateUser(
  id: string,
  updatedUser: User,
  token: string
): Promise<Response> {
  const formattedUpdatedUser = {
    ...updatedUser,
    dateOfBirth: dayjs(updatedUser.dateOfBirth).format('YYYY-MM-DD'), //Formatting
  };
  return fetch(`${userManagementServiceUrl}/api/v1/users/${id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      ...getHeaders(token).headers,
    },
    body: JSON.stringify(formattedUpdatedUser),
  }).then((res) => {
    if (res.status !== 204) throw new Error();

    return res;
  });
}

export async function deleteUser(id: string, token: string): Promise<Response> {
  return fetch(`${userManagementServiceUrl}/api/v1/users/${id}`, {
    method: 'DELETE',
    ...getHeaders(token),
  });
}

export async function refreshAuth(
  refreshToken: string
): Promise<AuthenticationResponse> {
  return fetch(`${userManagementServiceUrl}/api/v1/auth/refresh`, {
    ...getHeaders(refreshToken),
  }).then((res) => {
    if (!res.ok) throw new Error('Refresh failed');

    return res.json();
  });
}
