import { BookingList } from 'types/Booking';

const bookingServiceUrl = process.env.REACT_APP_BOOKING_SERVICE_URL;

function getHeaders(token: string) {
  return {
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
      'Access-Control-Allow-Origin': '*',
    },
  };
}

export function getAttendees(
  sessionId: string,
  token: string
): Promise<BookingList> {
  return fetch(`${bookingServiceUrl}/api/booking/${sessionId}`, {
    ...getHeaders(token),
  }).then((res) => {
    if (res.status !== 200) throw new Error();

    return res.json();
  });
}

export function book(
  userId: string,
  sessionId: string,
  token: string
): Promise<BookingList> {
  return fetch(`${bookingServiceUrl}/api/booking`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      ...getHeaders(token).headers,
    },
    body: JSON.stringify({
      userId: userId,
      sessionId: sessionId,
    }),
  }).then((res) => {
    if (res.status !== 201) throw new Error();

    return res.json();
  });
}

export function cancelBooking(
  userId: string,
  sessionId: string,
  token: string
): Promise<Response> {
  return fetch(`${bookingServiceUrl}/api/booking`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
      ...getHeaders(token).headers,
    },
    body: JSON.stringify({
      userId: userId,
      sessionId: sessionId,
    }),
  }).then((res) => {
    if (res.status !== 204) throw new Error();

    return res;
  });
}
