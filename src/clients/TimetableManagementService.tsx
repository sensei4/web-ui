import { Dayjs } from 'dayjs';
import { NonRecurringSession, RecurringSession } from 'types/Session';

const timetableManagementServiceUrl =
  process.env.REACT_APP_TIMETABLE_SERVICE_URL;

function getHeaders(token: string) {
  return {
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: 'application/json',
    },
  };
}

export async function getTimetable(
  from: Dayjs,
  to: Dayjs,
  token: string
): Promise<NonRecurringSession[]> {
  return fetch(
    `${timetableManagementServiceUrl}/api/v1/timetable?from=${from.format(
      'YYYY-MM-DD'
    )}&to=${to.format('YYYY-MM-DD')}`,
    getHeaders(token)
  ).then((res) => {
    if (!res.ok) throw new Error();

    return res.json();
  });
}

export async function getSession(
  id: string,
  token: string
): Promise<RecurringSession | NonRecurringSession> {
  return fetch(
    `${timetableManagementServiceUrl}/api/v1/sessions/${id}`,
    getHeaders(token)
  ).then((res) => {
    if (!res.ok) throw new Error('Unable to retrieve session');

    return res.json();
  });
}

export async function saveSession(
  value: NonRecurringSession | RecurringSession,
  token: string
): Promise<RecurringSession | NonRecurringSession> {
  return fetch(`${timetableManagementServiceUrl}/api/v1/sessions`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      ...getHeaders(token).headers,
    },
    body: JSON.stringify(value),
  }).then((res) => {
    if (!res.ok) throw new Error('Unable to save session');

    return res.json();
  });
}

export async function updateSession(
  id: string,
  value: NonRecurringSession | RecurringSession,
  token: string
): Promise<RecurringSession | NonRecurringSession> {
  return fetch(`${timetableManagementServiceUrl}/api/v1/sessions/${id}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      ...getHeaders(token).headers,
    },
    body: JSON.stringify(value),
  }).then((res) => {
    if (!res.ok) throw new Error('Unable to save session');

    return res.json();
  });
}

export async function deleteSession(
  id: string,
  token: string
): Promise<Response> {
  return fetch(`${timetableManagementServiceUrl}/api/v1/sessions/${id}`, {
    method: 'DELETE',
    headers: {
      ...getHeaders(token).headers,
    },
  }).then((res) => {
    if (!res.ok) throw new Error('Unable to save session');

    return res;
  });
}
