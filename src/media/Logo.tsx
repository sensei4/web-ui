import Sensei from '../media/full.png';

type LogoProps = {
  styles?: React.CSSProperties;
};
function Logo({ styles }: LogoProps) {
  return (
    <img
      alt='Logo'
      style={{
        margin: 'auto',
        objectFit: 'cover',
        maxWidth: '100%',
        maxHeight: '100%',
        background: 'white',
        ...styles,
      }}
      src={Sensei}
    />
  );
}

export default Logo;
