import { Button, Calendar, Layout, List, Modal, Row, Space } from 'antd';
import { getTimetable } from 'clients/TimetableManagementService';
import { Failure } from 'components/feedback/failure/Failure';
import { Loading } from 'components/feedback/loading/Loading';
import dayjs, { Dayjs } from 'dayjs';
import { useCallback, useContext, useEffect, useState } from 'react';
import { NonRecurringSession } from 'types/Session';
import './SessionManager.sass';
import { TokenContext } from 'components/protectedRoute/ProtectedRoute';
import { SessionEditor } from 'components/sessionEditor/SessionEditor';

export const SessionManager = () => {
  const token = useContext(TokenContext);
  const [sessions, setSessions] = useState<NonRecurringSession[]>([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [selectedDate, setSelectedDate] = useState<Date>(new Date());

  const updateSessions = useCallback(() => {
    setLoading(true);
    getTimetable(
      dayjs(selectedDate).startOf('month'),
      dayjs(selectedDate).endOf('month'),
      token
    )
      .then((json) => {
        setSessions(json);
        setLoading(false);
      })
      .catch(() => setError(true));
  }, [selectedDate, token]);

  useEffect(() => {
    updateSessions();
  }, [selectedDate, updateSessions]);

  const asListItem = (session: NonRecurringSession) => {
    return (
      <List.Item key={session.id}>
        <Button
          type='text'
          size='small'
          onClick={() => presentSessionEditModal(session)}
          style={{
            width: '100%',
            textAlign: 'start',
            color: 'rgb(52, 112, 252)',
          }}
        >{`(${session.startTime}) ${session.title}`}</Button>
      </List.Item>
    );
  };

  const presentSessionsModal = (
    date: Dayjs,
    sessions: NonRecurringSession[]
  ) => {
    Modal.info({
      title: date.format('DD MMMM YYYY'),
      content: (
        <List
          size='large'
          dataSource={sessions}
          renderItem={(session) => asListItem(session)}
        />
      ),
      okType: 'danger',
      okText: 'Close',
    });
  };

  const presentSessionEditModal = (session: NonRecurringSession) => {
    Modal.info({
      title: session.title ? session.title : 'New Session',
      content: (
        <SessionEditor
          sessionId={session.id}
          token={token}
        />
      ),
      okType: 'danger',
      okText: 'Close',
      onOk: () => updateSessions(),
    });
  };

  const dateCellRender = (value: Dayjs) => {
    const sessionsOnDay = sessions
      .filter(
        (session) =>
          value.format('YYYY-MM-DD') ===
          dayjs(session.date).format('YYYY-MM-DD')
      )
      .sort((a, b) =>
        dayjs(a.startTime, 'hh:mm').diff(dayjs(b.startTime, 'hh:mm'))
      );

    if (sessionsOnDay.length === 0) return <></>;

    if (sessionsOnDay.length > 3)
      return (
        <>
          <List
            className='events'
            // size=''
            dataSource={sessionsOnDay.slice(0, 2)}
            renderItem={(session) => asListItem(session)}
          />

          <Row
            align={'bottom'}
            justify={'end'}
          >
            <Button
              size='small'
              shape='round'
              style={{
                margin: '5px',
              }}
              disabled={value.isBefore(dayjs().subtract(1, 'day'))}
              onClick={() => presentSessionsModal(value, sessionsOnDay)}
            >
              More
            </Button>
          </Row>
        </>
      );

    return (
      <List
        className='events'
        dataSource={sessionsOnDay}
        renderItem={(session) => asListItem(session)}
      />
    );
  };

  if (loading) {
    return <Loading />;
  }

  if (error) {
    return <Failure />;
  }

  return (
    <Layout>
      <Space direction='vertical'>
        <Calendar
          mode='month'
          disabledDate={(date) => {
            return date.isBefore(dayjs().subtract(1, 'day'));
          }}
          dateCellRender={(date) => dateCellRender(date)}
          onChange={(date) => {
            if (
              !(
                date.month() === selectedDate.getMonth() &&
                date.year() === selectedDate.getFullYear()
              )
            )
              setSelectedDate(date.toDate());
          }}
        />
        <Row justify='end'>
          <Button
            type='primary'
            onClick={() => presentSessionEditModal({} as NonRecurringSession)}
          >
            Add session
          </Button>
        </Row>
      </Space>
    </Layout>
  );
};
