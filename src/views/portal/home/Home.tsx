import { Avatar, Layout, Row, Space, Typography } from 'antd';
import { UserContext } from 'components/protectedRoute/ProtectedRoute';
import { AdminInformationRow } from 'components/widgets/AdminInformationRow';
import { ProfileWidget } from 'components/widgets/ProfileWidget';
import { TimetableSummary } from 'components/widgets/TimetableSummary';
import dayjs from 'dayjs';
import { useContext } from 'react';
import { isAdmin } from 'types/User';

export const Home = () => {
  const user = useContext(UserContext);

  return (
    <Layout
      style={{
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
      }}
    >
      <Space
        direction='vertical'
        style={{ width: '100%' }}
      >
        <Row style={{ paddingBottom: '7px' }}>
          <Space align='end'>
            <Avatar size={'large'}>{user.firstName.substring(0, 1)}</Avatar>
            <Typography.Title
              level={2}
              style={{ margin: 0 }}
            >{`Hello ${user.firstName}`}</Typography.Title>
          </Space>
        </Row>
        {isAdmin(user) && <AdminInformationRow />}
        <Row>
          <Space
            style={{
              width: '100%',
              display: 'grid',
              // gridTemplateColumns: 'repeat(3, 33%)',
              gridTemplateColumns: `repeat(auto-fill,
              minmax(clamp(clamp(33%, (575px - 100vw)*1000, 50%), (400px - 100vw)*1000, 100%),
              1fr))`,

              // gridTemplateColumns: `repeat(auto-fit, minmax(200px, 1fr));`,
              gap: '10px',
              gridAutoFlow: 'row',
            }}
          >
            <ProfileWidget />
            {process.env.REACT_APP_TIMETABLE_SERVICE_URL && (
              <TimetableSummary
                date={dayjs()}
                isAdmin={isAdmin(user)}
              />
            )}
          </Space>
        </Row>
      </Space>
    </Layout>
  );
};
