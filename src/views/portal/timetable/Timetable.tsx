import { Layout, Row, Space, Timeline, Typography } from 'antd';
import { getTimetable } from 'clients/TimetableManagementService';
import { Failure } from 'components/feedback/failure/Failure';
import { Loading } from 'components/feedback/loading/Loading';
import { NoClasses } from 'components/feedback/noClasses/NoClasses';
import { TimetableItem } from 'components/timetableItem/TimetableItem';
import dayjs, { Dayjs } from 'dayjs';
import { useContext, useEffect, useState } from 'react';
import { NonRecurringSession } from 'types/Session';
import { TokenContext } from '../../../components/protectedRoute/ProtectedRoute';

export const Timetable = () => {
  const token = useContext(TokenContext);
  const [sessions, setSessions] = useState<NonRecurringSession[]>([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  function getAllDatesBetween(startDate: Dayjs, endDate: Dayjs): Dayjs[] {
    const dates: Dayjs[] = [];

    let currentDate = startDate;

    while (currentDate <= endDate) {
      dates.push(currentDate);
      currentDate = currentDate.add(1, 'day');
    }

    return dates;
  }

  useEffect(() => {
    getTimetable(dayjs(), dayjs().add(6, 'days'), token)
      .then((timetable) => setSessions(timetable))
      .catch((err) => setError(err))
      .finally(() => setLoading(false));
  }, [token]);

  if (loading) return <Loading />;
  if (error) return <Failure />;

  return (
    <Layout>
      <Typography.Title>Class Booking</Typography.Title>
      {getAllDatesBetween(dayjs(), dayjs().add(6, 'days')).map((date) => (
        <Space
          key={date.format('YYYY-MM-DD')}
          direction='vertical'
          style={{ display: 'flex' }}
        >
          <Row>
            <Typography.Title level={3}>
              {date.format('dddd DD MMM YYYY')}
            </Typography.Title>
          </Row>
          {sessions.filter(
            (session) =>
              dayjs(session.date).format('YYYY-MM-DD') ===
              date.format('YYYY-MM-DD')
          ).length > 0 ? (
            <Timeline
              items={sessions
                .filter(
                  (session) =>
                    dayjs(session.date).format('YYYY-MM-DD') ===
                    date.format('YYYY-MM-DD')
                )
                .sort((a, b) =>
                  dayjs(a.startTime, 'hh:mm').diff(dayjs(b.startTime, 'hh:mm'))
                )

                .map((session) => {
                  return {
                    children: <TimetableItem session={session} />,
                  };
                })}
            />
          ) : (
            <NoClasses />
          )}
        </Space>
      ))}
    </Layout>
  );
};
