import { UserAddOutlined } from '@ant-design/icons';
import {
  Avatar,
  Button,
  Descriptions,
  Layout,
  Modal,
  Row,
  Typography,
} from 'antd';
import { ContactEditor } from 'components/contactEditor/ContactEditor';
import { ContactSummary } from 'components/contactSummary/ContactSummary';
import ProfileEditor from 'components/profileEditor/ProfileEditor';
import {
  TokenContext,
  UserContext,
} from 'components/protectedRoute/ProtectedRoute';
import dayjs from 'dayjs';
import { useContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { EmergenyContact } from 'types/User';
import { DOB_FORMAT_LONG } from 'utils/Constants';
import './Profile.scss';

function Profile() {
  const initialUser = useContext(UserContext);
  const token = useContext(TokenContext);
  const [user, setUser] = useState(initialUser);
  const [editor, setEditor] = useState(false);
  const navigate = useNavigate();

  const presentEmergencyContactEditor = (contact?: EmergenyContact) => {
    Modal.warn({
      icon: <UserAddOutlined />,
      title: contact ? 'Edit Contact' : 'New Contact',
      content: (
        <ContactEditor
          contactToEdit={contact}
          currentUser={user}
          token={token}
        />
      ),
      onOk: () => {
        navigate(0); // Refreshing so new contact/changes to contact are displayed
      },
      closable: true,
      okType: 'default',
      okText: 'Done',
    });
  };

  useEffect(() => {}, [user]);

  if (editor)
    return (
      <ProfileEditor
        user={user}
        cancellable
        onCancel={() => setEditor(!editor)}
        onSuccess={(updatedUser) => {
          setUser(updatedUser);
          setEditor(!editor);
        }}
      />
    );

  return (
    <Layout className='profile'>
      <Row
        className='header'
        justify={'start'}
        align={'middle'}
        style={{ flexWrap: 'nowrap' }}
      >
        <Avatar
          size={48}
          style={{ minWidth: '48px' }}
        >
          {user?.firstName.at(0)}
        </Avatar>
        <Typography.Title
          level={2}
          style={{ margin: 'auto 12px auto' }}
        >
          {user?.firstName + ' ' + user?.lastName}{' '}
        </Typography.Title>
      </Row>
      <Descriptions
        layout='vertical'
        bordered
        column={{ xs: 1, md: 4 }}
        labelStyle={{
          fontWeight: 'bold',
        }}
      >
        <Descriptions.Item
          label='Date of Birth'
          span={4}
        >
          {dayjs(user.dateOfBirth).format(DOB_FORMAT_LONG)}
        </Descriptions.Item>
        <Descriptions.Item
          label='Email'
          span={4}
        >
          <a href={'mailto:' + user.emailAddress}>{user.emailAddress}</a>
        </Descriptions.Item>
        <Descriptions.Item
          label='Contact Number'
          span={4}
        >
          <a href={'tel:' + user.contactNumber}>{user.contactNumber}</a>
        </Descriptions.Item>
        <Descriptions.Item
          label='Emergency Contacts'
          className='contacts'
        >
          {user.emergencyContactList &&
            user.emergencyContactList.length > 0 && (
              <ContactSummary
                contacts={user.emergencyContactList}
                openEditor={(contact) => presentEmergencyContactEditor(contact)}
                onDelete={() => navigate(0)}
              ></ContactSummary>
            )}

          {(!user.emergencyContactList ||
            user.emergencyContactList.length < 2) && (
            <Button
              type='primary'
              onClick={() => presentEmergencyContactEditor()}
            >
              Add
            </Button>
          )}
        </Descriptions.Item>
      </Descriptions>
      <Row justify={'end'}>
        <Button
          type='default'
          size='large'
          onClick={() => setEditor(!editor)}
        >
          Edit
        </Button>
      </Row>
    </Layout>
  );
}

export default Profile;
