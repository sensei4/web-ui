import {
  CheckCircleFilled,
  ExclamationCircleFilled,
  MailFilled,
  MoreOutlined,
  PhoneFilled,
} from '@ant-design/icons';
import {
  Avatar,
  Button,
  Card,
  Dropdown,
  Layout,
  Modal,
  Row,
  Space,
  Statistic,
  Table,
  Typography,
  notification,
} from 'antd';
import { deleteUser, getAllUsers } from 'clients/UserManagementService';
import { Loading } from 'components/feedback/loading/Loading';
import ProfileEditor from 'components/profileEditor/ProfileEditor';
import {
  TokenContext,
  UserContext,
} from 'components/protectedRoute/ProtectedRoute';
import { NewUserCount } from 'components/widgets/NewUserCount';
import { useContext, useEffect, useState } from 'react';
import { EmergenyContact, User } from 'types/User';

export const UserAdmin = () => {
  const currentUser = useContext(UserContext);
  const token = useContext(TokenContext);
  const [loading, setLoading] = useState(true);
  const [users, setUsers] = useState<User[]>();
  const [api, contextHolder] = notification.useNotification();

  const presentEmergencyContactModal = (contact: EmergenyContact) => {
    Modal.info({
      title: contact.firstName + ' ' + contact.lastName,
      content: (
        <div>
          <Row style={{ paddingBottom: '1em' }}>
            <a href={'mailto:' + contact.emailAddress}>
              <MailFilled />
              {' ' + contact.emailAddress}
            </a>
          </Row>
          <Row>
            <a href={'tel:' + contact.contactNumber}>
              <PhoneFilled />
              {' ' + contact.contactNumber}
            </a>
          </Row>
        </div>
      ),
      onOk() {},
      closable: true,
      okType: 'danger',
      okText: 'Close',
    });
  };

  const presentEditModal = (user: User) => {
    Modal.info({
      title: 'Edit',
      content: (
        <ProfileEditor
          user={user}
          hideTitle
          onCancel={() => {}}
        />
      ),
      okType: 'default',
      okText: 'Done',
    });
  };

  const presentDeleteModal = (user: User, token: string) => {
    Modal.warning({
      title: 'Confirm Delete',
      content: (
        <Typography.Text>
          Are you sure you want to delete user: {user.firstName} {user.lastName}
          ?
        </Typography.Text>
      ),
      okType: 'danger',
      okText: 'Confirm',
      okCancel: true,
      onOk: () => {
        deleteUser(user.id, token)
          .then(() => {
            api.open({
              message: 'Success',
              description: 'User deleted',
              icon: <CheckCircleFilled style={{ color: 'green' }} />,
            });
            setUsers(users?.filter((item) => item.id !== user.id));
          })
          .catch(() => {
            api.open({
              message: 'Error',
              description: 'User could not be deleted, please try again later.',
              icon: <ExclamationCircleFilled style={{ color: 'red' }} />,
            });
          });
      },
    });
  };

  useEffect(() => {
    getAllUsers(token)
      .then((users) => {
        setUsers(users.filter((user) => user.id !== currentUser.id)); // Filtering out active user
        setLoading(false);
      })
      .catch((err) => console.log(err));
  }, [token, currentUser]);

  if (loading) return <Loading />;

  return (
    <Layout className='users'>
      {contextHolder}
      <Space direction='vertical'>
        <Row>
          <Space>
            <Card bordered={false}>
              <Statistic
                title='Total Users'
                value={users?.length}
              />
            </Card>
            {<NewUserCount days={7} />}
          </Space>
        </Row>
        <Table
          style={{
            overflowX: 'auto',
          }}
          columns={[
            {
              title: '',
              key: 'avatar',
              responsive: ['lg'],
              width: 1,
              render: (_, user) => <Avatar>{user.firstName.at(0)}</Avatar>,
            },
            {
              title: 'Name',
              key: 'name',
              render: (_, user) => (
                <Typography.Text>
                  {user.firstName} {user.lastName}
                </Typography.Text>
              ),
            },
            {
              title: 'Contact No.',
              key: 'contact',
              responsive: ['sm'],
              render: (_, user) => (
                <a href={'tel:' + user.contactNumber}>{user.contactNumber}</a>
              ),
            },
            {
              title: 'Email',
              key: 'email',
              responsive: ['sm'],
              render: (_, user) => (
                <a href={`mailto:${user.emailAddress}`}>{user.emailAddress}</a>
              ),
            },
            {
              title: 'Contact',
              key: 'contactActions',
              responsive: ['xs'],
              render: (_, user) => (
                <Space
                  wrap
                  direction='horizontal'
                  align='center'
                >
                  <Button
                    icon={<MailFilled />}
                    href={`mailto:${user.emailAddress}`}
                  ></Button>
                  <Button
                    icon={<PhoneFilled />}
                    href={`tel:${user.contactNumber}`}
                  ></Button>
                </Space>
              ),
            },
            {
              title: 'Emergency Contacts',
              key: 'contacts',
              responsive: ['sm'],
              render: (_, user) => (
                <Space wrap>
                  {user.emergencyContactList ? (
                    user.emergencyContactList.map((contact, i) => (
                      <Button
                        onClick={() => presentEmergencyContactModal(contact)}
                        key={i}
                      >
                        {contact.firstName} {contact.lastName}
                      </Button>
                    ))
                  ) : (
                    <Typography.Text disabled>No contacts</Typography.Text>
                  )}
                </Space>
              ),
            },
            {
              title: '',
              key: 'view',
              width: 1,
              responsive: ['xs'],
              render: (_, user) => (
                <Space>
                  <Dropdown
                    menu={{
                      items: [
                        {
                          label: 'Edit',
                          key: 'edit',
                          onClick: () => presentEditModal(user),
                        },
                        {
                          label: 'Delete',
                          key: 'delete',
                          danger: true,
                          onClick: () => presentDeleteModal(user, token),
                        },
                      ],
                    }}
                    trigger={['click']}
                  >
                    <MoreOutlined key={'ellipsis'} />
                  </Dropdown>
                </Space>
              ),
            },
            {
              key: 'actions',
              width: 1,
              responsive: ['sm'],
              render: (_, user) => (
                <Dropdown
                  menu={{
                    items: [
                      {
                        label: 'Edit',
                        key: 'edit',
                        onClick: () => presentEditModal(user),
                      },
                      {
                        label: 'Delete',
                        key: 'delete',
                        danger: true,
                        onClick: () => presentDeleteModal(user, token),
                      },
                    ],
                  }}
                  trigger={['click']}
                >
                  <MoreOutlined key={'ellipsis'} />
                </Dropdown>
              ),
            },
          ]}
          dataSource={users}
          rowKey='id' // {(user) => user.id}
          pagination={{
            position: ['bottomCenter'],
            pageSize: 5,
          }}
        />
      </Space>
    </Layout>
  );
};
