import { LogoutOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { Content } from 'antd/es/layout/layout';
import { useEffect } from 'react';
import '../postauth.scss';

type LogoutProps = {
  onLogout: () => void;
};

export const Logout = ({ onLogout }: LogoutProps) => {
  useEffect(() => {
    onLogout();
  }, [onLogout]);

  return (
    <Content className='postauth'>
      <div>
        <LogoutOutlined />
        <h1>Successfully signed out</h1>
        <h2>You can close this window or log in again</h2>
        <Button
          size='large'
          type='primary'
          href='/login'
        >
          Log in
        </Button>
      </div>
    </Content>
  );
};
