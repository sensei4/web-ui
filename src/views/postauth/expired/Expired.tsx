import { Button } from 'antd';
import { Content } from 'antd/es/layout/layout';
import { FieldTimeOutlined } from '@ant-design/icons';

export const Expired = () => {
  return (
    <Content className='postauth'>
      <div>
        <FieldTimeOutlined />
        <h1>Your session has expired!</h1>
        <h2>You can close this window or log in again</h2>
        <Button
          size='large'
          type='primary'
          href='/login'
        >
          Log in
        </Button>
      </div>
    </Content>
  );
};
