import {
  Alert,
  Button,
  Col,
  DatePicker,
  Form,
  Input,
  Row,
  Typography,
} from 'antd';
import { registerUser } from 'clients/UserManagementService';
import dayjs from 'dayjs';
import full from 'media/full.png';
import { useState } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { AuthenticationResponse } from 'types/AuthenticationResponse';
import { NewUser } from 'types/User';
import {
  CONTACT_NUMBER_REGEX,
  DOB_FORMAT_SHORT,
  EMAIL_REGEX,
} from 'utils/Constants';
import { sessionExists } from 'utils/SessionUtils';

type RegisterProps = {
  onSuccess: (auth: AuthenticationResponse) => void;
};

function Register({ onSuccess }: RegisterProps) {
  const { Title } = Typography;
  const [isLoading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string | undefined>();
  const navigate = useNavigate();

  function onFinish(values: any) {
    setLoading(true);
    setErrorMessage(undefined);
    let newUser: NewUser = {
      firstName: values.firstName,
      lastName: values.lastName,
      dateOfBirth: dayjs(values.dateOfBirth),
      emailAddress: values.emailAddress,
      contactNumber: values.contactNumber,
      password: values.password,
    };

    registerUser(newUser)
      .then((data: AuthenticationResponse) => {
        onSuccess(data);
        setLoading(false);
        navigate('/portal');
      })
      .catch((err) => {
        setErrorMessage(err.message);
        setLoading(false);
      });
  }

  if (sessionExists()) return <Navigate to='/portal' />;
  return (
    <div>
      <Row>
        <Col
          span={12}
          className='desktop-only'
        >
          <div id='background'>
            <div id='overlay'>
              <img
                src={full}
                alt=''
              />{' '}
            </div>
          </div>
        </Col>
        <Col
          span={12}
          className='half-page-form'
        >
          <Title>Register</Title>
          <Form
            name='basic'
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete='off'
            disabled={isLoading}
          >
            <Form.Item
              name='firstName'
              rules={[
                { required: true, message: 'You must enter your first name' },
              ]}
            >
              <Input placeholder='First name' />
            </Form.Item>

            <Form.Item
              name='lastName'
              rules={[
                { required: true, message: 'You must enter your surname' },
              ]}
            >
              <Input placeholder='Surname' />
            </Form.Item>

            <Form.Item
              name='emailAddress'
              rules={[
                {
                  required: true,
                  message: 'You must enter your email address',
                },
                {
                  pattern: EMAIL_REGEX,
                  message: 'You must enter a valid email address',
                },
              ]}
              // validateStatus={errorMessage ? 'error' : undefined}
              // help={errorMessage}
            >
              <Input placeholder='Email address' />
            </Form.Item>

            <Form.Item
              name='contactNumber'
              rules={[
                {
                  required: true,
                  message: 'You must enter your contact number',
                },
                {
                  pattern: CONTACT_NUMBER_REGEX,
                  message: 'You must enter a valid contact number',
                },
              ]}
            >
              <Input placeholder='Contact Number' />
            </Form.Item>

            <Form.Item
              name='password'
              rules={[
                {
                  required: true,
                  message: 'You must enter your password',
                },
                {
                  min: 8,
                  message: 'Password must be at least 8 characters in length',
                },
              ]}
            >
              <Input.Password placeholder='Password' />
            </Form.Item>

            <Form.Item
              name='confirmPassword'
              rules={[
                { required: true, message: 'You must confirm your password' },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error('Passwords must match'));
                  },
                }),
              ]}
            >
              <Input.Password placeholder='Confirm password' />
            </Form.Item>

            <Form.Item
              name='dateOfBirth'
              rules={[
                {
                  required: true,
                  message: 'You must enter your date of birth',
                },
              ]}
            >
              <DatePicker
                placeholder='Date of birth'
                format={DOB_FORMAT_SHORT}
              />
            </Form.Item>

            {errorMessage && (
              <Alert
                message={errorMessage}
                type='error'
                showIcon
                style={{ marginBottom: '24px' }}
              />
            )}
            <Form.Item>
              <Button
                type='primary'
                htmlType='submit'
              >
                Register
              </Button>
            </Form.Item>
            <Button href='/login'>Sign in</Button>
          </Form>
        </Col>
      </Row>
    </div>
  );
}

export default Register;
