import { Button, Checkbox, Col, Form, Input, Row, Typography } from 'antd';
import { authenticateUser } from 'clients/UserManagementService';
import '../preauth.scss';
import full from 'media/full.png';
import { useState } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { AuthenticationResponse } from 'types/AuthenticationResponse';
import { sessionExists } from 'utils/SessionUtils';

type loginProps = {
  onSuccess: (auth: AuthenticationResponse) => void;
};

function Login({ onSuccess }: loginProps) {
  const { Title } = Typography;
  const navigate = useNavigate();
  const [isLoading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState<string | undefined>();

  const onFinish = async (values: any) => {
    if (values.remember) {
      localStorage.setItem('login-username', values.username);
    } else {
      localStorage.removeItem('login-username');
    }

    setLoading(true);
    await authenticateUser(values.username, values.password)
      .then((response) => response.json())
      .then((data: AuthenticationResponse) => {
        onSuccess(data);
        navigate('/portal');
      })
      .catch((err) => {
        displayErrorMessage(err);
      })
      .finally(() => setLoading(false));
  };

  function displayErrorMessage(err: any) {
    if (err.response !== undefined) {
      switch (err.response.status) {
        case 404:
          setErrorMessage('Account not found.');
          break;
        case 406:
          setErrorMessage('Account details did not match.');
          break;
        default:
          setErrorMessage('Something went wrong, please try again');
          break;
      }
    } else {
      setErrorMessage('Something went wrong, please try again');
    }
  }

  // TODO
  if (sessionExists()) return <Navigate to='/portal' />;
  return (
    <div>
      <Row>
        <Col
          span={12}
          className='desktop-only'
        >
          <div id='background'>
            <div id='overlay'>
              <img
                src={full}
                alt=''
              />
            </div>
          </div>
        </Col>
        <Col
          span={12}
          className='half-page-form'
        >
          <Title>Login</Title>
          <Form
            name='login'
            initialValues={{
              username: localStorage.getItem('login-username') || '',
              remember: true,
            }}
            onFinish={onFinish}
            autoComplete='off'
            disabled={isLoading}
          >
            <Form.Item
              name='username'
              rules={[
                { required: true, message: 'You must enter your username' },
              ]}
              validateStatus={errorMessage ? 'error' : undefined}
            >
              <Input placeholder='Username' />
            </Form.Item>

            <Form.Item
              name='password'
              rules={[
                { required: true, message: 'You must enter your password' },
              ]}
              help={errorMessage}
              validateStatus={errorMessage ? 'error' : undefined}
            >
              <Input.Password placeholder='Password' />
            </Form.Item>

            <Form.Item
              labelAlign='left'
              name='remember'
              valuePropName='checked'
              className='remember'
            >
              <Checkbox className='checkbox'>Remember me</Checkbox>
            </Form.Item>

            <Form.Item>
              <Button
                type='primary'
                htmlType='submit'
              >
                Sign In
              </Button>
            </Form.Item>
            <Button
              type='default'
              href='/register'
            >
              Register
            </Button>
          </Form>
        </Col>
      </Row>
    </div>
  );
}

export default Login;
