import { RocketOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import { Content } from 'antd/lib/layout/layout';
import './NotFound.scss';

export default function NotFound() {
  return (
    <Content className='notFound'>
      <div>
        <RocketOutlined />
        <h1>Uh oh...</h1>
        <h2>We couldn't find the page you were looking for.</h2>
        <Button
          type='primary'
          href='/portal'
        >
          Back to safety
        </Button>
      </div>
    </Content>
  );
}
