// https://regexpattern.com/email-address/
export const EMAIL_REGEX: RegExp = new RegExp(
  // eslint-disable-next-line
  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
);
export const CONTACT_NUMBER_REGEX = new RegExp(/^(?:0|\+44)[0-9]{9,10}$/);

export const DOB_FORMAT_SHORT = 'D / M / YYYY';
export const DATE_FORMAT_SHORT = 'D/M/YYYY';
export const DOB_FORMAT_LONG = 'D MMMM YYYY';
