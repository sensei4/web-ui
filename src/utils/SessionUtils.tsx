import { AuthenticationResponse } from 'types/AuthenticationResponse';

const authKey = 'AUTH';

export function setSessionAuth(value: AuthenticationResponse) {
  sessionStorage.setItem(authKey, JSON.stringify(value));
}

export function clearSessionAuth() {
  sessionStorage.removeItem(authKey);
}

export function sessionExists() {
  return sessionStorage.getItem(authKey) != null;
}

export function getSessionAuth() {
  const storedValue = sessionStorage.getItem(authKey);
  if (storedValue) return JSON.parse(storedValue) as AuthenticationResponse;

  return null;
}
