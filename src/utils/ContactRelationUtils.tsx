export const RELATION_OPTIONS = [
  { value: 'parentOrGuardian', label: 'Parent / Guardian' },
  { value: 'spouse', label: 'Spouse' },
  { value: 'sibling', label: 'Sibling' },
];

export function getLabel(value: string) {
  const option = RELATION_OPTIONS.filter((option) => option.value === value);

  if (option.length > 0) {
    return option.at(0)?.label;
  }

  return value;
}
