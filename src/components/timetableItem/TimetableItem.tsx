import { Alert, Avatar, Button, Card, Row, Space, Typography } from 'antd';
import { book, cancelBooking, getAttendees } from 'clients/BookingService';
import {
  TokenContext,
  UserContext,
} from 'components/protectedRoute/ProtectedRoute';
import dayjs from 'dayjs';
import { useCallback, useContext, useEffect, useState } from 'react';
import { NonRecurringSession } from 'types/Session';

type TimetableItemProps = {
  session: NonRecurringSession;
  small?: boolean;
};
export const TimetableItem = ({ session, small }: TimetableItemProps) => {
  const user = useContext(UserContext);
  const token = useContext(TokenContext);
  const [attendees, setAttendees] = useState<String[]>();
  const [failedToBook, setFailedToBook] = useState(false);

  const updateAttendees = useCallback(() => {
    getAttendees(session.id, token).then((list) =>
      setAttendees(list.attendees)
    );
  }, [session.id, token]);

  useEffect(() => {
    updateAttendees();
  }, [session.id, token, updateAttendees]);

  return (
    <Card
      key={session.id}
      title={
        <Row
          style={{
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <Typography.Text
            style={{
              fontSize: small ? '14px' : '18px',
            }}
          >
            {`(${session.startTime}) ${session.title}`}
          </Typography.Text>
          <Space>
            <Avatar
              size='small'
              shape='square'
            >
              T
            </Avatar>
          </Space>
        </Row>
      }
      size='small'
    >
      {process.env.REACT_APP_BOOKING_SERVICE_URL ? (
        <Row
          style={{
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          {attendees && (
            <Avatar.Group
              maxCount={3}
              maxStyle={{
                backgroundColor: 'lightblue',
                color: 'white',
              }}
            >
              {attendees.map((participant) => (
                <Avatar
                  style={{
                    color: '#f56a00',
                    backgroundColor: '#fde3cf',
                  }}
                  key={participant.valueOf()}
                >
                  {participant.at(0)}
                </Avatar>
              ))}
            </Avatar.Group>
          )}
          {attendees?.includes(user.id) ? (
            <Button
              danger
              onClick={async () =>
                await cancelBooking(user.id, session.id, token).then(() =>
                  updateAttendees()
                )
              }
            >
              Cancel Booking
            </Button>
          ) : (
            <>
              {failedToBook && (
                <Alert
                  message='Could not book session.'
                  type='warning'
                  showIcon
                />
              )}
              <Button
                disabled={
                  dayjs(session.date).format('YYYY-MM-DD') ===
                    dayjs().format('YYYY-MM-DD') &&
                  dayjs(session.startTime, 'HH:mm').isBefore(dayjs())
                }
                onClick={async () => {
                  setFailedToBook(false);
                  await book(user.id, session.id, token)
                    .then(() => updateAttendees())
                    .catch(() => setFailedToBook(true));
                }}
                size={small ? 'small' : 'middle'}
              >
                Book
              </Button>
            </>
          )}
        </Row>
      ) : (
        <Typography.Text>
          Bookings are not required for this class.
        </Typography.Text>
      )}
    </Card>
  );
};
