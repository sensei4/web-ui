import { Menu } from 'antd';
import { Footer } from 'antd/es/layout/layout';
import React, { useContext, useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { getMenuItems, memberMenuItems } from '../MenuItems';
import { UserContext } from 'components/protectedRoute/ProtectedRoute';

export const MobileNav = () => {
  const user = useContext(UserContext);
  const location = useLocation();
  const navigate = useNavigate();
  const [current, setCurrent] = useState(location.pathname);
  const [menuItems, setMenuItems] = useState(memberMenuItems);

  useEffect(() => {
    setMenuItems(getMenuItems(user));
  }, [user]);

  useEffect(() => {
    if (location) {
      if (current !== location.pathname) {
        setCurrent(location.pathname);
      }
    }
  }, [location, current]);

  return (
    <Footer
      className='mobile-only'
      style={{
        width: '100vw',
        flexDirection: 'row',
        justifyContent: 'center',
        border: 'none',
        position: 'fixed',
        bottom: 0,
        left: 0,
        zIndex: 1000,
      }}
    >
      <Menu
        id='mobileNav'
        mode='horizontal'
        selectedKeys={[current]}
        subMenuCloseDelay={100}
        triggerSubMenuAction='click'
        style={{
          width: '100vw',
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          border: 'none',
          position: 'fixed',
          bottom: 0,
          left: 0,
        }}
        items={menuItems.map((item) => {
          return {
            key: item.path,
            icon: React.createElement(item.icon),
            onClick: () => navigate(item.path),
            label: item.text,
          };
        })}
      />
    </Footer>
  );
};
