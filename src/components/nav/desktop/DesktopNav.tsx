import { Layout, Menu } from 'antd';
import React, { useContext, useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import Logo from '../../../media/Logo';
import { getMenuItems, memberMenuItems } from '../MenuItems';
import { UserContext } from 'components/protectedRoute/ProtectedRoute';

function DesktopNav() {
  const user = useContext(UserContext);
  const location = useLocation();
  const { Sider } = Layout;
  const navigate = useNavigate();
  const [current, setCurrent] = useState(location.pathname);
  const [menuItems, setMenuItems] = useState(memberMenuItems);

  useEffect(() => {
    setMenuItems(getMenuItems(user));
  }, [user]);

  useEffect(() => {
    if (location) {
      if (current !== location.pathname) {
        setCurrent(location.pathname);
      }
    }
  }, [location, current]);

  return (
    <Sider
      className='desktop-only'
      collapsible
      breakpoint='md'
      theme='light'
      style={{
        overflowY: 'hidden',
      }}
    >
      <Logo styles={{ padding: '5%' }} />
      <Menu
        mode='inline'
        selectedKeys={[current]}
        style={{
          height: '100%',
          border: 'none',
        }}
        items={menuItems.map((item) => {
          return {
            key: item.path,
            icon: React.createElement(item.icon),
            label: item.text,
            onClick: () => navigate(item.path),
          };
        })}
      />
    </Sider>
  );
}

export default DesktopNav;
