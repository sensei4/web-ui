import {
  CalendarOutlined,
  HomeOutlined,
  LogoutOutlined,
  ProfileOutlined,
  UserOutlined,
  FieldTimeOutlined,
} from '@ant-design/icons';
import { User } from 'types/User';

export const memberMenuItems = [
  {
    icon: HomeOutlined,
    text: 'Home',
    path: '/portal',
    enabled: true,
  },
  {
    icon: ProfileOutlined,
    text: 'Profile',
    path: '/portal/profile',
    enabled: true,
  },
  {
    icon: CalendarOutlined,
    text: 'Timetable',
    path: '/portal/timetable',
    enabled: allPresent([process.env.REACT_APP_TIMETABLE_SERVICE_URL]),
  },
];
const adminOnlyMenuItems = [
  {
    icon: UserOutlined,
    text: 'Manage Users',
    path: '/portal/users',
    enabled: true,
  },
  {
    icon: FieldTimeOutlined,
    text: 'Manage Sessions',
    path: '/portal/sessions',
    enabled: allPresent([process.env.REACT_APP_TIMETABLE_SERVICE_URL]),
  },
];
const logout = {
  icon: LogoutOutlined,
  text: 'Logout',
  path: '/logout',
  enabled: true,
};

function allPresent(dependencies: (string | undefined)[]): boolean {
  return dependencies.filter((dep) => dep === undefined).length === 0;
}

export function getMenuItems(user: User) {
  var items = memberMenuItems;
  if (user.userType === 'ADMIN') items = items.concat(adminOnlyMenuItems);

  return items.filter((item) => item.enabled).concat(logout);
}
