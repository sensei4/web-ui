import {
  Alert,
  Button,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Row,
  Select,
  Space,
  TimePicker,
} from 'antd';
import {
  deleteSession,
  getSession,
  saveSession,
  updateSession,
} from 'clients/TimetableManagementService';
import { getAllUsers } from 'clients/UserManagementService';
import { Failure } from 'components/feedback/failure/Failure';
import { Loading } from 'components/feedback/loading/Loading';
import dayjs from 'dayjs';
import { useEffect, useState } from 'react';
import { NonRecurringSession, RecurringSession } from 'types/Session';
import { User } from 'types/User';

interface SessionEditorProps {
  sessionId: string;
  token: string;
}
export const SessionEditor = ({ sessionId, token }: SessionEditorProps) => {
  const [requestInProgress, setRequestInProgress] = useState(false);
  const [complete, setComplete] = useState(false);
  const [error, setError] = useState();
  const [coaches, setCoaches] = useState<User[]>();
  const [type, setType] = useState<'recurring' | 'non-recurring'>('recurring');
  const [session, setSession] = useState<
    RecurringSession | NonRecurringSession
  >();

  useEffect(() => {
    if (sessionId) {
      if (sessionId.includes(':')) {
        setType('recurring');

        const id = sessionId.substring(0, sessionId.indexOf(':'));
        getSession(id, token).then((returned) => {
          setSession(returned);
        });
      } else {
        setType('non-recurring');
        getSession(sessionId, token).then((returned) => {
          setSession(returned);
        });
      }
    }

    getAllUsers(token).then((users) => {
      setCoaches(
        users.filter(
          (user) => user.userType === 'COACH' || user.userType === 'ADMIN'
        )
      );
    });
  }, [sessionId, token]);

  function isDeletable(
    session: RecurringSession | NonRecurringSession
  ): boolean {
    if (type === 'recurring')
      return dayjs().isBefore((session as RecurringSession).schedule.from);

    return (
      dayjs().isBefore((session as NonRecurringSession).date) ||
      (dayjs().format('YYYY-MM-DD') ===
        dayjs((session as NonRecurringSession).date).format('YYYY-MM-DD') &&
        dayjs().isBefore(
          dayjs((session as NonRecurringSession).startTime, 'HH:mm')
        ))
    );
  }

  function onFinish(values: any) {
    const updatedSession: NonRecurringSession | RecurringSession =
      values.type === 'recurring'
        ? {
            ...values,
            schedule: {
              ...values.schedule,
              from: values.schedule.from.format('YYYY-MM-DD'),
              until: values.schedule.until.format('YYYY-MM-DD'),
              startTime: values.schedule.startTime.format('HH:mm'),
            },
          }
        : {
            ...values,
            date: values.date.format('YYYY-MM-DD'),
            startTime: values.startTime.format('HH:mm'),
          };

    const upsert = (session: RecurringSession | NonRecurringSession) => {
      if (updatedSession.id === undefined) {
        return saveSession(session, token);
      } else {
        return updateSession(session.id, updatedSession, token);
      }
    };

    upsert(updatedSession)
      .then(() => {
        setComplete(true);
      })
      .catch((err) => setError(err))
      .finally(() => setRequestInProgress(false));
  }

  if ((sessionId && !session) || !coaches) return <Loading />;

  if (coaches) {
    return (
      <>
        <Form
          name='session'
          onFinish={onFinish}
          layout='vertical'
          disabled={requestInProgress || complete || error}
        >
          {session && (
            <Form.Item
              name='id'
              label='ID'
              initialValue={session?.id}
              rules={[{ required: true }]}
            >
              <Input
                disabled
                placeholder='ID'
              />
            </Form.Item>
          )}
          <Form.Item
            name='title'
            label='Title'
            initialValue={session ? session.title : undefined}
            rules={[
              { required: true, message: 'You must enter a session title' },
            ]}
          >
            <Input placeholder='Title' />
          </Form.Item>
          <Form.Item
            name='coach'
            label='Coach'
            initialValue={session?.coach}
            rules={[{ required: true, message: 'You must select a coach' }]}
          >
            <Select
              placeholder='Coach'
              options={coaches.map((coach) => {
                return {
                  value: coach.id,
                  label: `${coach.firstName} ${coach.lastName}`,
                };
              })}
            />
          </Form.Item>
          <Form.Item
            label='Duration'
            rules={[{ required: true, message: 'You must select a coach' }]}
          >
            <Space.Compact>
              <Form.Item
                name={['duration', 'value']}
                initialValue={session?.duration?.value}
                noStyle
                rules={[
                  {
                    required: true,
                    message: 'You must enter a duration value',
                  },
                ]}
              >
                <InputNumber placeholder='Value' />
              </Form.Item>
              <Form.Item
                name={['duration', 'unit']}
                initialValue={session?.duration?.unit}
                noStyle
                rules={[
                  {
                    required: true,
                    message: 'You must select a duration unit',
                  },
                ]}
              >
                <Select
                  placeholder='Select unit'
                  options={[
                    { value: 'MINUTES', label: 'Minute(s)' },
                    { value: 'HOURS', label: 'Hour(s)' },
                  ]}
                />
              </Form.Item>
            </Space.Compact>
          </Form.Item>
          <Form.Item
            name='type'
            label='Type'
            initialValue={type}
            rules={[{ required: true, message: 'You must select a coach' }]}
          >
            <Select
              placeholder='Type'
              options={[
                { value: 'recurring', label: 'Recurring' },
                { value: 'non-recurring', label: 'Non-recurring' },
              ]}
              onChange={(val) => setType(val)}
            />
          </Form.Item>

          {type === 'recurring' && (
            <>
              <Form.Item
                label='Frequency'
                initialValue={
                  (session as RecurringSession)?.schedule?.frequency
                }
                name={['schedule', 'frequency']}
                // noStyle
                rules={[
                  { required: true, message: 'You must select a frequency' },
                ]}
              >
                <Select
                  placeholder='Select frequency'
                  options={[
                    { value: 'DAILY', label: 'Daily' },
                    { value: 'WEEKLY', label: 'Weekly' },
                    { value: 'MONTHLY', label: 'Monthly' },
                  ]}
                />
              </Form.Item>
              <Space.Compact>
                <Form.Item
                  label='From'
                  name={['schedule', 'from']}
                  initialValue={dayjs(
                    (session as RecurringSession)?.schedule?.from
                  )}
                  rules={[
                    { required: true, message: 'You must select a date' },
                  ]}
                >
                  <DatePicker
                    allowClear={false}
                    disabled={
                      session &&
                      dayjs().isAfter(
                        (session as RecurringSession).schedule.from
                      )
                    }
                    disabledDate={(date) =>
                      dayjs().subtract(1, 'day').isAfter(date)
                    }
                  />
                </Form.Item>
                <Form.Item
                  label='Until'
                  name={['schedule', 'until']}
                  initialValue={dayjs(
                    (session as RecurringSession)?.schedule?.until
                  )}
                  // noStyle
                  rules={[
                    { required: true, message: 'You must select a date' },
                  ]}
                >
                  <DatePicker
                    allowClear={false}
                    disabled={
                      session &&
                      dayjs().isAfter(
                        (session as RecurringSession).schedule.until
                      )
                    }
                    disabledDate={(date) =>
                      dayjs().subtract(1, 'day').isAfter(date)
                    }
                  />
                </Form.Item>
              </Space.Compact>
              <Form.Item
                label='Start Time'
                name={['schedule', 'startTime']}
                initialValue={
                  session
                    ? dayjs(
                        (session as RecurringSession).schedule?.startTime,
                        'HH:mm'
                      )
                    : dayjs()
                }
                // noStyle
                rules={[{ required: true, message: 'You must select a date' }]}
              >
                <TimePicker format={'HH:mm'} />
              </Form.Item>
            </>
          )}
          {type === 'non-recurring' && (
            <>
              <Space.Compact>
                <Form.Item
                  label='Date'
                  name={'date'}
                  initialValue={
                    session
                      ? dayjs((session as NonRecurringSession).date)
                      : dayjs()
                  }
                  // noStyle
                  rules={[
                    { required: true, message: 'You must select a date' },
                  ]}
                >
                  <DatePicker
                    disabledDate={(date) =>
                      dayjs().subtract(1, 'day').isAfter(date)
                    }
                  />
                </Form.Item>
                <Form.Item
                  label='Start Time'
                  name={'startTime'}
                  initialValue={
                    session
                      ? dayjs(
                          (session as NonRecurringSession).startTime,
                          'HH:mm'
                        )
                      : dayjs()
                  }
                  // noStyle
                  rules={[
                    { required: true, message: 'You must select a date' },
                  ]}
                >
                  <TimePicker format={'HH:mm'} />
                </Form.Item>
              </Space.Compact>
            </>
          )}
          <Row justify={'end'}>
            <Space>
              {session && isDeletable(session) && (
                <Button
                  onClick={() => {
                    setRequestInProgress(true);
                    deleteSession(session.id, token)
                      .catch((error) => setError(error))
                      .then(() => setComplete(true));
                  }}
                >
                  Delete
                </Button>
              )}
              <Button
                type='primary'
                htmlType='submit'
              >
                Save
              </Button>
            </Space>
          </Row>
        </Form>
        {complete && (
          <Alert
            style={{
              width: '100%',
              marginTop: '12px',
            }}
            message='Done'
            type='success'
            showIcon
          />
        )}
        {error && (
          <Alert
            style={{
              width: '100%',
              marginTop: '12px',
            }}
            message='Request failed'
            type='error'
            showIcon
          />
        )}
      </>
    );
  }

  return <Failure />;
};
