import { Alert, Button, Form, Input, Select, Space } from 'antd';
import { updateUser } from 'clients/UserManagementService';
import { useState } from 'react';
import { EmergenyContact, User } from 'types/User';
import { CONTACT_NUMBER_REGEX, EMAIL_REGEX } from 'utils/Constants';
import { RELATION_OPTIONS } from 'utils/ContactRelationUtils';

type ContactEditorProps = {
  contactToEdit?: EmergenyContact;
  currentUser: User;
  token: string;
};

export const ContactEditor = ({
  contactToEdit,
  currentUser,
  token,
}: ContactEditorProps) => {
  const [contactToReplace, setContactToReplace] = useState(contactToEdit);
  const [requestFailed, setRequestFailed] = useState(false);
  const [changesMade, setChangesMade] = useState(false);

  function saveChanges(value: EmergenyContact) {
    let updatedUser: User;
    if (currentUser.emergencyContactList) {
      if (contactToReplace)
        updatedUser = {
          ...currentUser,
          emergencyContactList: currentUser.emergencyContactList
            .filter((contact) => contact !== contactToReplace)
            .concat(value),
        };
      else
        updatedUser = {
          ...currentUser,
          emergencyContactList: currentUser.emergencyContactList.concat(value),
        };
    } else {
      updatedUser = {
        ...currentUser,
        emergencyContactList: [value],
      };
    }

    updateUser(currentUser.id, updatedUser, token)
      .then(() => {
        setContactToReplace(value);
        setChangesMade(false);
        setRequestFailed(false);
      })
      .catch(() => setRequestFailed(true));
  }

  return (
    <Form
      name='editContact'
      initialValues={contactToEdit ? contactToEdit : {}}
      layout='vertical'
      onFinish={(values) => saveChanges(values)}
    >
      <Form.Item
        name={'firstName'}
        label='First Name'
        rules={[{ required: true, message: 'You must enter their first name' }]}
      >
        <Input
          placeholder='First name'
          onChange={() => setChangesMade(true)}
        />
      </Form.Item>
      <Form.Item
        name={'lastName'}
        label='Last Name'
        rules={[{ required: true, message: 'You must enter their last name' }]}
      >
        <Input
          placeholder='Last name'
          onChange={() => {
            setChangesMade(true);
          }}
        />
      </Form.Item>
      <Form.Item
        name='emailAddress'
        label='Email Address'
        rules={[
          {
            required: true,
            message: 'You must enter their email address',
          },
          {
            pattern: EMAIL_REGEX,
            message: 'You must enter a valid email address',
          },
        ]}
      >
        <Input
          placeholder='Email address'
          onChange={() => setChangesMade(true)}
        />
      </Form.Item>
      <Form.Item
        name='contactNumber'
        label='Contact Number'
        rules={[
          {
            required: true,
            message: 'You must enter their contact number',
          },
          {
            pattern: CONTACT_NUMBER_REGEX,
            message: 'You must enter a valid contact number',
          },
        ]}
      >
        <Input
          placeholder='Contact number'
          onChange={() => setChangesMade(true)}
        />
      </Form.Item>
      <Form.Item
        name='relation'
        label='Relation'
        rules={[
          {
            required: true,
            message: 'You must select this persons relation to you',
          },
        ]}
      >
        <Select
          options={RELATION_OPTIONS}
          allowClear
          onChange={() => setChangesMade(true)}
        />
      </Form.Item>
      <Space
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          flexDirection: 'row',
          justifyContent: 'end',
        }}
      >
        {requestFailed && (
          <Alert
            message='Something went wrong, please try again.'
            type='error'
            showIcon
            style={{ marginBottom: '24px' }}
          />
        )}
        <Button
          type='primary'
          htmlType='submit'
          disabled={!changesMade || requestFailed}
        >
          Save
        </Button>
      </Space>
    </Form>
  );
};
