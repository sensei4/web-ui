import { getUser, refreshAuth } from 'clients/UserManagementService';
import { Loading } from 'components/feedback/loading/Loading';
import jwtDecode from 'jwt-decode';
import { createContext, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { AuthenticationResponse } from 'types/AuthenticationResponse';
import { TokenData } from 'types/TokenData';
import { User } from 'types/User';
import { getSessionAuth, setSessionAuth } from 'utils/SessionUtils';

type ProtectedRouteProps = {
  initialAuth: AuthenticationResponse | undefined;
  onRefresh: (auth: AuthenticationResponse) => void;
  onExpiry: () => void;
  children: JSX.Element;
};

function isExpired(tokenData: TokenData): boolean {
  return new Date() >= new Date(tokenData.exp * 1000);
}

export const UserContext = createContext<User>({} as User);
export const TokenContext = createContext<string>('');
export default function ProtectedRoute({
  initialAuth,
  onRefresh,
  onExpiry,
  children,
}: ProtectedRouteProps) {
  const [user, setUser] = useState<User>();
  const [token, setToken] = useState<string>();
  const navigate = useNavigate();

  useEffect(() => {
    function setTokenAndUser(auth: AuthenticationResponse) {
      const tokenData = jwtDecode(auth.access) as TokenData;
      if (isExpired(tokenData)) {
        const refreshTokenData = jwtDecode(auth.refresh) as TokenData;
        if (isExpired(refreshTokenData)) {
          onExpiry();
          navigate('/expired');
        } else {
          refreshAuth(auth.refresh)
            .then((updatedAuth) => {
              onRefresh(updatedAuth);
              setSessionAuth(updatedAuth);
            })
            .catch(() => {
              onExpiry();
              navigate('/expired');
            });
        }
      }

      setToken(auth.access);
      getUser(tokenData.sub, auth.access).then((user) => {
        setUser(user);
      });
    }

    const existingSession = getSessionAuth();
    if (existingSession) {
      setTokenAndUser(existingSession);
    } else if (initialAuth) {
      setTokenAndUser(initialAuth);
      setSessionAuth(initialAuth);
    } else {
      navigate('/login');
    }
  }, [initialAuth, navigate, onRefresh, onExpiry]);

  if (token && user)
    return (
      <TokenContext.Provider value={token}>
        <UserContext.Provider value={user}>{children}</UserContext.Provider>
      </TokenContext.Provider>
    );

  return <Loading />;
}
