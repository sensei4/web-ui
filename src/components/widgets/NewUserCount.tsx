import { ArrowUpOutlined } from '@ant-design/icons';
import { Card, Statistic } from 'antd';
import { getAllUsers } from 'clients/UserManagementService';
import dayjs from 'dayjs';
import { useContext, useEffect, useState } from 'react';
import { TokenContext } from '../protectedRoute/ProtectedRoute';

type NewUserCountProps = {
  style?: React.CSSProperties;
  days: number;
};
export const NewUserCount = ({ style, days }: NewUserCountProps) => {
  const token = useContext(TokenContext);
  const [count, setCount] = useState(0);

  useEffect(() => {
    getAllUsers(token).then((users) =>
      setCount(
        users.filter((user) =>
          dayjs(user.created).isAfter(dayjs().subtract(days, 'days'))
        ).length
      )
    );
  }, [token, days]);

  return (
    <Card
      style={{
        minWidth: '125px',
        ...style,
      }}
    >
      <Statistic
        title={`New Users (${days} days)`}
        value={count}
        prefix={<ArrowUpOutlined />}
        valueStyle={count > 0 ? { color: '#3f8600' } : {}}
      />
    </Card>
  );
};
