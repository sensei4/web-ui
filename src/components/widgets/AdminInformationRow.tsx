import { Row, Space, Typography } from 'antd';
import { NewUserCount } from './NewUserCount';

export const AdminInformationRow = () => {
  return (
    <Row
      style={{
        background: 'grey',
        padding: '12px',
        transform: 'translateX(-12px)',
        width: 'calc(100% + 24px)',
      }}
    >
      <Typography.Title
        level={5}
        style={{
          margin: 0,
          color: 'white',
          width: '100%',
          paddingBottom: '7px',
        }}
      >{`Admin Information`}</Typography.Title>
      <Row>
        <Space>
          <NewUserCount days={7} />
          <NewUserCount days={30} />
        </Space>
      </Row>
    </Row>
  );
};
