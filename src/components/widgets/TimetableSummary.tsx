import { Card } from 'antd';
import { getTimetable } from 'clients/TimetableManagementService';
import { NoClasses } from 'components/feedback/noClasses/NoClasses';
import { TokenContext } from 'components/protectedRoute/ProtectedRoute';
import { TimetableItem } from 'components/timetableItem/TimetableItem';
import { Dayjs } from 'dayjs';
import { useContext, useEffect, useState } from 'react';
import { NonRecurringSession } from 'types/Session';
import { DATE_FORMAT_SHORT } from 'utils/Constants';

type TimetableSummaryProps = {
  date: Dayjs;
  isAdmin: boolean;
};

export const TimetableSummary = ({ date, isAdmin }: TimetableSummaryProps) => {
  const token = useContext(TokenContext);
  const [sessions, setSessions] = useState<NonRecurringSession[]>([]);

  useEffect(() => {
    getTimetable(date, date, token).then((sessions) => setSessions(sessions));
  }, [token, date]);

  if (sessions.length > 0)
    return (
      <Card
        title={`${date.format(DATE_FORMAT_SHORT)}`}
        bodyStyle={{
          padding: '7px',
        }}
      >
        {sessions.map((session) => (
          <TimetableItem
            session={session}
            small
          ></TimetableItem>
        ))}
      </Card>
    );

  return (
    <Card
      title={`Sessions (${date.format(DATE_FORMAT_SHORT)})`}
      bodyStyle={{
        padding: '0px',
      }}
    >
      <NoClasses />
    </Card>
  );
};
