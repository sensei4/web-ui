import { CalendarOutlined, MailFilled, PhoneFilled } from '@ant-design/icons';
import { Avatar, Button, Card, Row, Space, Typography } from 'antd';
import Meta from 'antd/es/card/Meta';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { useContext } from 'react';
import { UserContext } from '../protectedRoute/ProtectedRoute';

export const ProfileWidget = (style?: React.CSSProperties) => {
  const user = useContext(UserContext);
  dayjs.extend(relativeTime);

  return (
    <Card
      hoverable
      style={{
        ...style,
        minWidth: 'fit-content',
        width: '100%',
        height: '100%',
      }}
      cover={
        <Avatar
          shape='square'
          size='large'
          style={{
            width: '100%',
            borderRadius: 0,
          }}
        >
          {user?.firstName.at(0)}
        </Avatar>
      }
    >
      <Meta
        title={`${user.firstName} ${user.lastName}`}
        style={{
          paddingBottom: '12px',
        }}
      />
      <Space direction='vertical'>
        <Row>
          <a href={'mailto:' + user.emailAddress}>
            <MailFilled />
            {' ' + user.emailAddress}
          </a>
        </Row>
        <Row>
          <a href={'tel:' + user.contactNumber}>
            <PhoneFilled />
            {' ' + user.contactNumber}
          </a>
        </Row>
        <Row>
          <Typography.Text>
            <CalendarOutlined />
            {` Joined ${dayjs(user.created).from(dayjs())}`}
          </Typography.Text>
        </Row>
        <Row justify='end'>
          <Button href='/portal/profile'>Edit</Button>
        </Row>
      </Space>
    </Card>
  );
};
