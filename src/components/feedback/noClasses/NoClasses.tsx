import { Result, Row } from 'antd';
import { MehOutlined } from '@ant-design/icons';

import React from 'react';

export const NoClasses = () => {
  return (
    <Row
      justify={'center'}
      style={{
        backgroundColor: 'white',
        borderRadius: '8px',
        marginLeft: '26px',
        marginBottom: '20px',
      }}
    >
      <Result
        icon={<MehOutlined />}
        title='No classes scheduled...'
      />
    </Row>
  );
};
