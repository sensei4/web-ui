import { Result } from 'antd';
import { Content } from 'antd/es/layout/layout';

export const Failure = () => {
  return (
    <Content className='content'>
      <Result
        status='error'
        title='Failed to load...'
        subTitle='This page could not be loaded. Please contact your system administrator if this issue persists.'
      ></Result>
    </Content>
  );
};
