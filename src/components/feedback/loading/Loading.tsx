import { Layout, Row, Spin } from 'antd';
import React from 'react';

export const Loading = () => {
  return (
    <Layout>
      <Row
        justify='center'
        align='middle'
      >
        <Spin
          tip='Loading...'
          size='large'
          style={{
            marginTop: '12px',
          }}
        />
      </Row>
    </Layout>
  );
};
