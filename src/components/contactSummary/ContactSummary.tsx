import { DeleteOutlined, MailFilled, PhoneFilled } from '@ant-design/icons';
import { Button, Descriptions, Popconfirm, Row } from 'antd';
import ButtonGroup from 'antd/es/button/button-group';
import { updateUser } from 'clients/UserManagementService';
import {
  TokenContext,
  UserContext,
} from 'components/protectedRoute/ProtectedRoute';
import { useContext } from 'react';
import { EmergenyContact } from 'types/User';
import { getLabel } from 'utils/ContactRelationUtils';

type ContactSummaryProps = {
  openEditor: (contact: EmergenyContact) => void;
  onDelete: () => void;
  contacts: EmergenyContact[];
};
export const ContactSummary = ({
  openEditor,
  contacts,
  onDelete,
}: ContactSummaryProps) => {
  const user = useContext(UserContext);
  const token = useContext(TokenContext);
  return (
    <Descriptions
      layout='vertical'
      bordered
      column={{ xs: 1, md: 4 }}
      size='small'
    >
      {contacts?.map((contact, i) => (
        <Descriptions.Item
          label={`${contact.firstName} ${contact.lastName} (${getLabel(
            contact.relation
          )})`}
          span={1}
          key={i}
        >
          <Row style={{ paddingBottom: '1em' }}>
            <a href={'mailto:' + contact.emailAddress}>
              <MailFilled />
              {' ' + contact.emailAddress}
            </a>
          </Row>
          <Row>
            <a href={'tel:' + contact.contactNumber}>
              <PhoneFilled />
              {' ' + contact.contactNumber}
            </a>
          </Row>
          <Row justify={'end'}>
            <ButtonGroup>
              <Popconfirm
                okType='danger'
                icon={<DeleteOutlined />}
                title='Confirm'
                description='Delete emergency contact?'
                onConfirm={() => {
                  updateUser(
                    user.id,
                    {
                      ...user,
                      emergencyContactList: user.emergencyContactList.filter(
                        (listItem) => {
                          return listItem !== contact;
                        }
                      ),
                    },
                    token
                  ).then(() => onDelete());
                }}
                okText='Yes'
                cancelText='No'
              >
                <Button
                  danger
                  type='primary'
                >
                  Delete
                </Button>
              </Popconfirm>
              <Button onClick={() => openEditor(contact)}>Edit</Button>
            </ButtonGroup>
          </Row>
        </Descriptions.Item>
      ))}
    </Descriptions>
  );
};
