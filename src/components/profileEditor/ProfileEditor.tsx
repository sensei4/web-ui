import {
  Alert,
  Button,
  DatePicker,
  Form,
  Input,
  Layout,
  Space,
  Typography,
} from 'antd';
import { updateUser } from 'clients/UserManagementService';
import dayjs from 'dayjs';
import { useContext, useState } from 'react';
import { Navigate } from 'react-router-dom';
import { User } from 'types/User';
import { DOB_FORMAT_SHORT } from 'utils/Constants';
import { TokenContext } from '../protectedRoute/ProtectedRoute';
import './profileEditor.sass';

type ProfileEditorProps = {
  user: User;
  hideTitle?: boolean;
  cancellable?: boolean;
  onCancel?: () => void;
  onSuccess?: (updatedUser: User) => void;
};

function ChangesWereMade(user: User, values: any): boolean {
  return (
    user.firstName !== values.firstName ||
    user.lastName !== values.lastName ||
    user.emailAddress !== values.emailAddress ||
    user.contactNumber !== values.contactNumber ||
    dayjs(values.dateOfBirth).format(DOB_FORMAT_SHORT) !==
      dayjs(user.dateOfBirth).format(DOB_FORMAT_SHORT)
  );
}

function ProfileEditor({
  user,
  hideTitle = false,
  cancellable = false,
  onCancel = () => {},
  onSuccess = () => {},
}: ProfileEditorProps) {
  const [error, setError] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const token = useContext(TokenContext);

  if (!user) return <Navigate to={'/login'} />;

  function onFinish(values: any) {
    setError(false);
    setDisabled(true);

    if (ChangesWereMade(user, values)) {
      updateUser(user.id, { ...user, ...values }, token)
        .then(() => {
          onSuccess({ ...user, ...values });
        })
        .catch((err) => {
          setError(true);
        });
    } else {
      onCancel();
    }

    setDisabled(false);
  }

  return (
    <Layout className='profile editor'>
      {!hideTitle && (
        <Typography.Title level={2}>Edit Profile</Typography.Title>
      )}
      <Form
        name='editProfile'
        onFinish={onFinish}
        layout='vertical'
        disabled={disabled}
      >
        <Form.Item
          name='firstName'
          label='First Name'
          initialValue={user.firstName}
          rules={[
            { required: true, message: 'You must enter your first name' },
          ]}
        >
          <Input placeholder='First name' />
        </Form.Item>

        <Form.Item
          name='lastName'
          label='Last Name'
          initialValue={user.lastName}
          rules={[{ required: true, message: 'You must enter your surname' }]}
        >
          <Input placeholder='Surname' />
        </Form.Item>

        <Form.Item
          name='emailAddress'
          label='Email Address'
          initialValue={user.emailAddress}
          rules={[
            {
              required: true,
              message: 'You must enter your email address',
            },
          ]}
        >
          <Input placeholder='Email address' />
        </Form.Item>

        <Form.Item
          name='dateOfBirth'
          label='Date of Birth'
          initialValue={dayjs(user.dateOfBirth)}
          rules={[
            {
              required: true,
              message: 'You must enter your date of birth',
            },
          ]}
        >
          <DatePicker
            placeholder='Date of birth'
            format={DOB_FORMAT_SHORT}
            style={{
              width: '100%',
            }}
          />
        </Form.Item>

        <Form.Item
          name='contactNumber'
          label='Contact Number'
          initialValue={user.contactNumber}
          rules={[
            {
              required: true,
              message: 'You must enter your email address',
            },
          ]}
        >
          <Input placeholder='Contact number' />
        </Form.Item>

        <Form.Item>
          {error && (
            <Alert
              message='Something went wrong, please try again.'
              type='error'
              showIcon
              style={{ marginBottom: '24px' }}
            />
          )}
          <Space
            style={{
              display: 'flex',
              flexWrap: 'wrap',
              flexDirection: 'row',
              justifyContent: 'end',
            }}
          >
            {cancellable && <Button onClick={onCancel}>Cancel</Button>}
            <Button
              type='primary'
              htmlType='submit'
            >
              Save
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </Layout>
  );
}

export default ProfileEditor;
