import { Breadcrumb, Layout } from 'antd';
import 'antd/dist/reset.css';
import { Home } from 'views/portal/home/Home';
import { MobileNav } from 'components/nav/mobile/MobileNav';
import { UserAdmin } from 'views/portal/userAdmin/userAdmin';
import { Expired } from 'views/postauth/expired/Expired';
import { Logout } from 'views/postauth/logout/Logout';
import Register from 'views/preauth/register/Register';
import { useState } from 'react';
import { Navigate, Route, Routes, useLocation } from 'react-router-dom';
import { AuthenticationResponse } from 'types/AuthenticationResponse';
import { clearSessionAuth } from 'utils/SessionUtils';
import './App.sass';
import NotFound from './views/notFound/NotFound';
import DesktopNav from './components/nav/desktop/DesktopNav';
import Profile from './views/portal/profile/Profile';
import ProtectedRoute from './components/protectedRoute/ProtectedRoute';
import Login from './views/preauth/login/Login';
import { SessionManager } from 'views/portal/sessionManager/SessionManager';
import { Timetable } from 'views/portal/timetable/Timetable';
import { Header } from 'antd/es/layout/layout';
import Logo from 'media/Logo';

function App() {
  const { Content } = Layout;
  const [auth, setAuth] = useState<AuthenticationResponse>();

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Routes>
        <Route
          path=''
          element={<Navigate to='portal' />} // Redirect base path to '/portal', portal will redirect to login page if user is not logged in
        />
        <Route
          path='login'
          element={
            <Login onSuccess={(authorisation) => setAuth(authorisation)} />
          }
        />
        <Route
          path='register'
          element={
            <Register onSuccess={(authorisation) => setAuth(authorisation)} />
          }
        />
        <Route
          path='logout'
          element={
            <Logout
              onLogout={() => {
                setAuth(undefined);
                clearSessionAuth();
              }}
            />
          }
        />
        <Route
          path='expired'
          element={<Expired />}
        />
        <Route
          path='portal/*'
          element={
            <ProtectedRoute
              initialAuth={auth}
              onRefresh={(authorisation) => setAuth(authorisation)}
              onExpiry={() => {
                setAuth(undefined);
                clearSessionAuth();
              }}
            >
              <>
                <DesktopNav />
                <Layout
                  className='portal-content'
                  style={{
                    overflowY: 'auto',
                    overflowX: 'hidden',
                    height: '100%',
                  }}
                >
                  <Header
                    style={{
                      background: 'white',
                      marginBottom: '7px',
                      boxShadow: '0px 1px lightgrey',
                    }}
                    className='mobile-only'
                  >
                    <Logo />
                  </Header>
                  <Content
                    style={{
                      minHeight: 280,
                      padding: '12px',
                    }}
                  >
                    <Breadcrumb
                      style={{ marginBottom: '12px' }}
                      items={useLocation()
                        .pathname.split('/')
                        .slice(1)
                        .map((urlPart) => {
                          return {
                            key: urlPart,
                            title:
                              urlPart === 'portal'
                                ? 'Home'
                                : urlPart.charAt(0).toUpperCase() +
                                  urlPart.slice(1),
                          };
                        })}
                    />
                    <Routes>
                      <Route
                        path=''
                        element={<Home />}
                      />
                      <Route
                        path='profile'
                        element={<Profile />}
                      />
                      <Route
                        path='users'
                        element={<UserAdmin />}
                      />
                      <Route
                        path='sessions'
                        element={<SessionManager />}
                      />
                      <Route
                        path='timetable'
                        element={<Timetable />}
                      />
                      <Route
                        path='payments'
                        element={<h1>Payments</h1>}
                      />
                      <Route
                        path='announcements'
                        element={<h1>Announcements</h1>}
                      />
                      <Route
                        path='*'
                        element={<Navigate to='/not-found' />}
                      />
                    </Routes>
                  </Content>
                  <MobileNav />
                </Layout>
              </>
            </ProtectedRoute>
          }
        />
        <Route
          path='not-found'
          element={<NotFound />}
        />
        <Route
          path='*'
          element={<Navigate to='not-found' />}
        />
      </Routes>
    </Layout>
  );
}

export default App;
