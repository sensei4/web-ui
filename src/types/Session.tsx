import { Dayjs } from 'dayjs';

type Duration = {
  value: number;
  unit: 'MINUTES' | 'HOURS';
};

type Schedule = {
  frequency: 'DAILY' | 'WEEKLY' | 'MONTHLY';
  from: Dayjs;
  until: Dayjs;
  startTime: string;
};

type Session = {
  id: string;
  type: 'recurring' | 'non-recurring';
  title: string;
  coach: string;
  duration: Duration;
};

export type RecurringSession = Session & {
  schedule: Schedule;
};

export type NonRecurringSession = Session & {
  date: Dayjs;
  startTime: Dayjs;
};
