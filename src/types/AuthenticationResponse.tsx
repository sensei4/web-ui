export type AuthenticationResponse = {
  access: string;
  refresh: string;
};
