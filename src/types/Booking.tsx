export type BookingRequest = {
  sessionId: string;
  userId: string;
};

export type BookingList = {
  sessionId: string;
  attendees: string[];
};
