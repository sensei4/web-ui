import { Dayjs } from 'dayjs';

export type BaseUser = {
  firstName: string;
  lastName: string;
  dateOfBirth: Dayjs;
  emailAddress: string;
  contactNumber: string;
};

export type User = BaseUser & {
  id: string;
  emergencyContactList: EmergenyContact[];
  created: Dayjs;
  userType: 'MEMBER' | 'COACH' | 'ADMIN';
};

export type NewUser = BaseUser & {
  password: string;
};

export type EmergenyContact = {
  firstName: string;
  lastName: string;
  emailAddress: string;
  contactNumber: string;
  relation: string;
};

export function isAdmin(user: User) {
  return user.userType === 'ADMIN';
}
