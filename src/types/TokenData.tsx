export type TokenData = {
  sub: string;
  exp: number;
  rol: string;
};
