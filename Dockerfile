FROM node:alpine AS build

WORKDIR /app

# Copying package.json to build container 
# and installing dependencies
COPY package.json ./
RUN npm install 

# Copying all files in web-ui directory to the container 
# and building the service
COPY . ./
RUN npm run build

# Using nginx image to serve the built app
FROM nginx:1.23.3-alpine AS run

# Removing default nginx configuration files
# and copying configuration to container
RUN rm /etc/nginx/conf.d/default.conf
RUN rm /usr/share/nginx/html/50x.html
COPY nginx/nginx.conf /etc/nginx/conf.d

# Copying the built app to nginx container
COPY --from=build /app/build /usr/share/nginx/html

# Setting port and starting nginx service
EXPOSE 80
ENTRYPOINT ["nginx", "-g", "daemon off;"]
